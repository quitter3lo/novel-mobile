import Vue from 'vue'
import Mint from 'mint-ui'
import 'mint-ui/lib/style.css'
import './assest/reset.css'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import VueLazyload from 'vue-lazyload'

import IndexComponent from './components/index.vue'
import NovelComponent from './components/novel.vue'
import SettingComponent from './components/setting.vue'
import ChapterComponent from './components/chapter.vue'
import SearchComponent from './components/search.vue'
import StylesComponent from './components/style.vue'
import DisclaimerComponent from './components/disclaimer.vue'

Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(Mint);
Vue.use(VueLazyload);
Vue.use(VueLazyload, {
    preLoad: 1.3,
    error: 'assest/cover.png',
    loading: 'assest/cover.png',
    attempt: 1
});

const routes = [
    {path: '/novel/:id', component: NovelComponent},
    {path: '/chapter/:id/:novel_id', component: ChapterComponent},
    {path: '/chapter/:id', component: ChapterComponent},
    {path: '/search', component: SearchComponent},
    {path: '/styles', component: StylesComponent},
    {path: '/setting', component: SettingComponent},
    {path: '/disclaimer', component: DisclaimerComponent},
    {path: '*', component: IndexComponent},
];

const router = new VueRouter({
    routes
})

const app = new Vue({
    router: router,
}).$mount('#app')
